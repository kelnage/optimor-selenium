from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

class O2PriceScraper:
    """Load the O2 international price page in Firefox (via selenium) and fetch prices for a call to a given countries landline phone."""
    international_price_url = "http://international.o2.co.uk/internationaltariffs/calling_abroad_from_uk"
    search_field_id = "countryName"
    paymonthly_button_id = "paymonthly"
    landline_td_xpath = "//table[@id='standardRatesTable']/tbody/tr/td[text()='Landline']/following-sibling::td"
    wait_seconds = 10
    
    def __init__(self):
        """Set up Firefox and load O2 international price page"""
        self.driver = webdriver.Firefox()
        self.driver.get(O2PriceScraper.international_price_url)
    
    def getPrice(self, country):
        """Get the price for calling a landline phone in the given country from an O2 mobile.

        Args:
          country (str): the name of the country
        """
        try:
            searchfield = WebDriverWait(self.driver, O2PriceScraper.wait_seconds).until(EC.element_to_be_clickable((By.ID, O2PriceScraper.search_field_id)))
            searchfield.click() # clicking on the field causes it to be emptied by the page's Javascript
            searchfield.send_keys(country)
            searchfield.send_keys(Keys.RETURN)
            paymonthly = WebDriverWait(self.driver, O2PriceScraper.wait_seconds).until(EC.element_to_be_clickable((By.ID, O2PriceScraper.paymonthly_button_id)))
            paymonthly.click()
            landline_rate = WebDriverWait(self.driver, O2PriceScraper.wait_seconds).until(EC.presence_of_element_located((By.XPATH, O2PriceScraper.landline_td_xpath)))
            return landline_rate.text
        except TimeoutException:
            raise Error("The structure of the O2 page has changed or connection could not be established")

    def __del__(self):
        self.driver.quit()

if __name__ == "__main__":
    countries = ["Canada", "Germany", "Iceland", "Pakistan", "Singapore", "South Africa"]
    scraper = O2PriceScraper()
    print("\n".join([country + " costs " + scraper.getPrice(country) for country in countries]))
